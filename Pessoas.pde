import processing.serial.*;
import cc.arduino.*;

Arduino arduino;
PShape pessoas[];
float position;
color corPessoas[], bg, svg;
int x, y, startTime, duration,
    quantidade, deslocation,
    nroPes = 6, ledPin = 13,
    pinoSensorPIR = 7, val = 0,
    pirState = Arduino.LOW;

long previousMillis = 0;
long interval = 1000;

long previousMillis2 = 0;
long interval2 = 5000;

void setup() {
  size(1024, 550);
  frameRate(10);

  if (frame != null)
    frame.setResizable(true);

  pessoas = new PShape[nroPes];
  corPessoas = new color[nroPes];

  for (
    int pessoa = 0; pessoa < pessoas.length; pessoa++) {
    pessoas[pessoa] = loadShape("p" + pessoa + ".svg");
    pessoas[pessoa].disableStyle();
  }

  deslocation = width;
  position = 1;
  quantidade = 0;
  svg = 0;

  println(Arduino.list());

  arduino = new Arduino(this, Arduino.list()[0], 9600);
  arduino.pinMode(4, Arduino.INPUT);

  arduino.pinMode(ledPin, Arduino.OUTPUT);
  arduino.pinMode(pinoSensorPIR, Arduino.INPUT);

  colorMode(HSB, 359, 99, 99);
  bg = color(0, 0, 99);
  corPessoas[svg] = color(209, 0, 99);

  fill(corPessoas[svg]);
  noStroke();
  smooth();

  background(bg);
}

void draw() {
  background(bg);
  pes();
  luz();
  pir();
}

void pes() {
  switch (quantidade) {
  case 0:
    break;
  case 1:
    //pessoas[0].disableStyle();
    shape(pessoas[0], (
    x+=position)%deslocation + 0, y, 200, height);
    break;
  case 2:
    shape(pessoas[0], (
    x+=position)%deslocation + 0, y, 200, height);
    shape(pessoas[1], (
    x+=position)%deslocation + 200, y, 500, height);
    break;
  case 3:
    shape(pessoas[2], (
    x+=position)%deslocation + 100, y, 300, height);
    shape(pessoas[1], (
    x+=position)%deslocation + 200, y, 500, height);
    break;
  case 4:
    shape(pessoas[0], (
    x+=position)%deslocation + 0, y, 200, height);
    shape(pessoas[1], (
    x+=position)%deslocation + 200, y, 500, height);
    shape(pessoas[2], (
    x+=position)%deslocation + 100, y, 300, height);
    shape(pessoas[3], (
    x+=position)%deslocation + 300, y, 200, height);
    break;
  case 5:
    shape(pessoas[0], (
    x+=position)%deslocation + 0, y, 200, height);
    shape(pessoas[1], (
    x+=position)%deslocation + 200, y, 500, height);
    shape(pessoas[2], (
    x+=position)%deslocation + 100, y, 300, height);
    shape(pessoas[3], (
    x+=position)%deslocation + 300, y, 200, height);
    shape(pessoas[4], (
    x+=position)%deslocation - 150, y, 200, height);
    break;
  case 6:
    shape(pessoas[2], (
    x+=position)%deslocation + 100, y, 300, height);
    shape(pessoas[1], (
    x+=position)%deslocation + 200, y, 500, height);
    shape(pessoas[0], (
    x+=position)%deslocation +   0, y, 200, height);
    shape(pessoas[3], (
    x+=position)%deslocation + 400, y, 200, height);
    shape(pessoas[4], (
    x+=position)%deslocation - 150, y, 200, height);
    shape(pessoas[5], (
    x+=position)%deslocation - 380, y, 200, height);
    break;
  default:
    break;
  }
}

void luz() {
  long currentMillis2 = millis();

  if (currentMillis2 - previousMillis2 > interval2) {

    if (arduino.analogRead(0) < 500) {
      // ta claro
      bg = color(
        floor(random(100)),
        floor(random(57)),
        floor(random(58)));
      corPessoas[svg+=1%corPessoas.length-1] = color(
            floor(random(75)),
            floor(random(50, 88)),
            floor(random(50, 99)));
      fill(corPessoas[svg]);
    } else if (arduino.analogRead(0) > 900) {
      // ta escuro
      bg = color(
        floor(random(0)),
        floor(random(0)),
        floor(random(99)));
      corPessoas[svg+=1%corPessoas.length-1] = color(
        floor(random(175)),
        floor(random(10, 25)),
        floor(random(0, 43)));
      fill(corPessoas[svg]);
    }
    previousMillis2 = currentMillis2;
  }
}

void pir() {
  long currentMillis = millis();

  if (currentMillis - previousMillis > interval) {

    val = arduino.digitalRead(pinoSensorPIR);

    if (val == Arduino.HIGH) {
      arduino.digitalWrite(ledPin, Arduino.HIGH);  // ON
      if (pirState == Arduino.LOW) {
        println("Motion detected!");
        pirState = Arduino.HIGH;
        if (quantidade < pessoas.length) {
          quantidade++;
        }
        else {
          quantidade = 0;
        }
      }
    } 
    else {
      arduino.digitalWrite(ledPin, Arduino.LOW); // OFF
      if (pirState == Arduino.HIGH) {
        println("Motion ended!");
        pirState = Arduino.LOW;
      }
    }
    previousMillis = currentMillis;
  }
}