Pavilinho
=========

Intervenção que fizemos no pavilinho como projeto da disciplina Atelier Livre – Tecnologias Interativas e Processos de Criação ARQ 5682 [5] – Atelier Livre / Arquitetura e Urbanismo UFSC / Prof. José Kós e Erica Mattos.


![Fotos Intervenção](pav1.jpg "Intervenção Pavilinho")
![Fotos Intervenção](pav2.jpg "Intervenção Pavilinho")
![Fotos Intervenção](pav3.jpg "Intervenção Pavilinho")
![Fotos Intervenção](pav4.jpg "Intervenção Pavilinho")


Instalação
=========

```git clone https://github.com/tonussi/pavilinho```

> Compile main.tex no seu editor de latex preferido e pronto
> Seu pdf da documentação.

Dependências
=========

> Utilizamos uma fonte chamada Libertine.
> Portanto, para compilar corretamente o main.tex
> você precisa instalar texlive-fonts-extra.

```sudo apt-get install texlive-fonts-extra```

> Essa linha de comando provavelmente irá instalar outras dependências


Equipe
=========

- Andressa **Xavier** [1]
- Arthur **Pickcius** [3]
- Marcela **Karam** [1]
- Peterson **Oliveira** [2]
- Lucas **Tonussi** [2]
- Conrado **Balvedi** [4]

Contribuidores
=========

- Daniel Spillere [dansku]
- Diego Fagundes [diegofagundes]
- Ramiro Polla [ramiropolla]

Espaço
=========

- ARQ 12
- Tarrafa Hackerspace [6]

[1]: http://www.arq.ufsc.br/      "Arquitetura e Urbanismo"
[2]: http://www.inf.ufsc.br/      "Ciência da Computação"
[3]: http://automacao.ufsc.br/    "Engenharia de Controle e Automação"
[4]: http://www.eps.ufsc.br/      "Engenharia de Produção Civil"
[5]: http://tecnologiasinterativas.wordpress.com/ "Tecnologias Interativas"
[6]: http://tarrafa.net/blog/ "Tarrafa Hackerspace"
[ramiropolla]: https://github.com/ramiropolla "Ramiro Polla"
[dansku]: https://github.com/dansku "Daniel Spillere"
[diegofagundes]: https://github.com/diegofagundes "Diego Fagundes"
